Title: Mythological Landscapes and Real Places: Using Virtual Reality to Investigate
the Perception of Sacred Space in the Ancient City of Memphis

Authors: Nevio Danelon and David J. Zielinski

Conference: Ancient Egypt and New Technology 2019

Abstract:
Memphis was one of the most illustrious and cosmopolitan metropolises of the
ancient world. Strategically located at the vertex of the Nile delta, it was the Old
Kingdom capital of Egypt. Its lifespan ranged from the beginning of the 3rd
millennium BC to the 5th century AD, making it one of the longest-lived cities of
the world. Memphis has almost completely disappeared, leaving impressive
pyramid complexes as the only evidence of its past existence.

Most of the information we know about its topography comes from ancient
sources that described Memphis both intentionally and accidentally. These
sources include classical historians who provided detailed accounts about the
topography of the city during the Late Period and Graeco-Roman Egypt. Among
Egyptian sources, the Memphite Theology is intriguing because it describes places
that were considered related to the mythological Egyptian imaginary but that may
reflect real places. Collating spatial information from textual sources and
comparing it with the morphology of the actual site, a consistent cityscape image
resurfaces. Lake Acherusia, the Abaton of Osiris, the fortress of the White Wall
and the temple of Ptah may eventually find their places in a predictive map.

We created a virtual reality (VR) application that helps us visualize the Memphite
landscape and conduct embodied explorations. We built our application using the
"Unity" game engine and deployed it on the low-cost "Oculus Go" portable VR
display system. 

Built version of the app is available on SideQuest:
https://sidequestvr.com/app/1841