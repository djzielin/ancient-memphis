﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;

[System.Serializable]
public class AllPlaces
{
   public string name;
   public string wall_text_left;
   public string wall_image_left;
   public float wall_image_left_aspect_ratio;

   public string wall_text_right;
   public string wall_image_right;
   public float wall_image_right_aspect_ratio;

   public List<SinglePlace> data = new List<SinglePlace>();
}

[System.Serializable]
public class SinglePlace
{
   public string name;
   public double lat;
   public double lon;
   public string icon;
   public float height;
   public float horiz_offset;
   public string menu;
}

public class LoadPlacesJSON : MonoBehaviour //TODO, this should really be refactored as PlaceMarkManager
{
   public AllPlaces ourPlaces;
   public bool setup=false;
   public GameObject MapObject;
   AbstractMap _map;
   public GameObject placePrototype;
   public GameObject userHead;
   bool iconsActivated = false;

   GameObject[] ourPlaceObjects;

   // Use this for initialization
   void Start()
   {
      Load();
      print("number of placemarks: " + ourPlaces.data.Count);
      _map = MapObject.GetComponent<AbstractMap>();

      //DumpTest();
      SetupPhysicalIcons();
   }

   void Load()
   {
      TextAsset txt = Resources.Load<TextAsset>("MemphisPlacemarks"); //ommit .json extension
      if (txt == null)
      {
         Debug.LogError("couldn't load text file!");
         return;
      }
      string filecontent = txt.text;
      ourPlaces = JsonUtility.FromJson<AllPlaces>(filecontent);
   }

   void DumpTest()
   {
      WholeMenu ourMenu = new WholeMenu();
      ourMenu.schematic_maps.Add(new SingleSchematic());


      string output = JsonUtility.ToJson(ourMenu);
      print(output);
      string path = "Assets/Resources/ptah_test.json";

      //Write some text to the test.txt file
      StreamWriter writer = new StreamWriter(path, true);
      writer.Write(output);
      writer.Close();
   }

   public void TurnAllOn()
   {
      foreach (GameObject g in ourPlaceObjects)
      {
         g.transform.Find("IconHolder").gameObject.SetActive(true);
         g.transform.Find("Cone").gameObject.SetActive(true);
      }

      iconsActivated = true;
   }
   public void TurnAllOff()
   {
      foreach (GameObject g in ourPlaceObjects)
      {
         g.transform.Find("IconHolder").gameObject.SetActive(false);
         g.transform.Find("Cone").gameObject.SetActive(false);
         g.transform.Find("Text").gameObject.SetActive(false);
      }

      iconsActivated = false;
   }

   bool AnyHits(GameObject g)
   {
      //print("checking visibility for: " + g);
      Vector3[] allTestPoints = new Vector3[9];

      //check all points in the bounding box/
      //https://answers.unity.com/questions/29797/how-to-get-8-vertices-from-bounds-properties.html

      GameObject icon = g.transform.Find("IconHolder").gameObject;
      Renderer rend = icon.GetComponent<Renderer>();

      allTestPoints[0] = rend.bounds.center;
      allTestPoints[1] = rend.bounds.center - rend.bounds.extents * 0.9f;
      allTestPoints[2] = rend.bounds.center + rend.bounds.extents * 0.9f;

      allTestPoints[3] = new Vector3(allTestPoints[1].x, allTestPoints[1].y, allTestPoints[2].z);
      allTestPoints[4] = new Vector3(allTestPoints[1].x, allTestPoints[2].y, allTestPoints[1].z);
      allTestPoints[5] = new Vector3(allTestPoints[2].x, allTestPoints[1].y, allTestPoints[1].z);
      allTestPoints[6] = new Vector3(allTestPoints[1].x, allTestPoints[2].y, allTestPoints[2].z);
      allTestPoints[7] = new Vector3(allTestPoints[2].x, allTestPoints[1].y, allTestPoints[2].z);
      allTestPoints[8] = new Vector3(allTestPoints[2].x, allTestPoints[2].y, allTestPoints[1].z);

      foreach (Vector3 pos in allTestPoints)
      {
         Ray r = new Ray(userHead.transform.position, (pos - userHead.transform.position).normalized);
         RaycastHit[] ourHits = Physics.RaycastAll(r, Mathf.Infinity, 1 << 13);
         if (ourHits.Length > 1)
            return true;
      }

      return false;
   }

   void SetupPhysicalIcons()
   {
      ourPlaceObjects = new GameObject[ourPlaces.data.Count];

      int i = 0;
      foreach (SinglePlace s in ourPlaces.data)
      {
         //print("setting up: " + s.name);
         Vector2d latlong = new Vector2d(s.lat, s.lon);
         Vector3 pos = _map.GeoToWorldPosition(latlong, true);

         GameObject ourPlace = Instantiate(placePrototype);
         ourPlace.name = s.name;

         ourPlace.GetComponent<PlaceMark>().targetPosition = pos; //store for later
         ourPlace.GetComponent<PlaceMark>().placeInfo = s;
         ourPlace.GetComponent<PlaceMark>().SetupTexture();
         ourPlace.GetComponent<PlaceMark>().ourLoader = this;
         ourPlace.GetComponent<PlaceMark>().LoadReadingListJSON();


         Vector3 shiftedPos = pos + new Vector3(s.horiz_offset, s.height, 0f); //pull y value from file
         ourPlace.transform.position = shiftedPos;

         //ourPlace.transform.parent = _map.gameObject.transform.parent.transform;
         ourPlace.SetActive(true);

         ourPlaceObjects[i] = ourPlace;
         i++;
      }

      
      foreach (GameObject g in ourPlaceObjects)
      {
         /* Vector3 originalPosition = g.transform.position;

          if (AnyHits(g)) //eventually just hardcode the placemark locations
          {
             for (int t = 0; t < 20; t++)
             {
                //print("Visibility problem with: " + g.name + " Iteration: " + t);
                //g.transform.position = originalPosition + new Vector3(0f, 0f, -0.05f * t); //try moving forward?
                //if (AnyHits(g) == false)
                //    break;

                g.transform.position = originalPosition + new Vector3(-0.05f * t, 0f, 0f); //try moving left?
                if (AnyHits(g) == false)
                   break;

                g.transform.position = originalPosition + new Vector3(0f, 0.05f * t, 0f); //try moving up?
                if (AnyHits(g) == false)
                   break;

                g.transform.position = originalPosition + new Vector3(-0.05f * t, 0.05f * t, 0f); //try moving both?
                if (AnyHits(g) == false)
                   break;
             }
          }*/
         g.GetComponent<PlaceMark>().ComputeMoreExactTargetPosition();
         g.GetComponent<PlaceMark>().SetupPointer();
      }
      

      /*GameObject prot=GameObject.Find("Proteus Enclosure"); //manually move this over. TODO: just hardcode all of them, instead of relying on auto-layout
      if(prot != null)
      {
          prot.transform.Translate(0.1f, 0f, 0f);
          prot.GetComponent<PlaceMark>().SetupPointer();
      }*/

      setup = true;
   }

   private void Update()
   {
      //experiments in trying to fix icon overlap

      /*
      if (Camera.main == null) //cameras not setup yet
         return;

      Vector3 camPos = Camera.main.transform.position;
      camPos.y = 0; //don't want to pitch text

      if (iconsActivated)
      {
         List<PlaceMark> ourList = new List<PlaceMark>();

         foreach (GameObject p in ourPlaceObjects)
         {
            PlaceMark ourPlaceMark = p.GetComponent<PlaceMark>();
            Vector3 placePos = ourPlaceMark.targetPosition;
            placePos.y = 0; //don't want to pitch text

            Vector3 placeInRefToCam = placePos - camPos;

            float magnitude = placeInRefToCam.magnitude;
            float degrees = Mathf.Atan2(placeInRefToCam.z, placeInRefToCam.x) * Mathf.Rad2Deg;

            ourPlaceMark.degree = degrees;
            ourPlaceMark.magnitude = magnitude;

            ourList.Add(ourPlaceMark);
         }

         ourList.Sort(PlaceMark.SortByDegree);

         foreach (PlaceMark p in ourList)
         {
            print("before adjustment: " + p.gameObject.name + " degrees: " + p.degree);
         }

         for (int e = 0; e < 5; e++)
         {
            int noProblemCount = 0;
            for (int i = 0; i < ourList.Count; i++)
            {
               float prevDegree = 0;
               float currentDegree = 0;
               float nextDegree = 0;

               int prevIndex = i - 1;
               if (prevIndex < 0)
               {
                  prevIndex += ourList.Count;
                  prevDegree -= 360;
               }
               PlaceMark pPrev = ourList[prevIndex];

               PlaceMark p = ourList[i];

               int nextIndex = i + 1;
               if (nextIndex >= ourList.Count)
               {
                  nextIndex -= ourList.Count;
                  nextDegree += 360;
               }
               PlaceMark pNext = ourList[(i + 1) % ourList.Count];

               prevDegree += pPrev.degree;
               currentDegree += p.degree;
               nextDegree += pNext.degree;

               print("Need to analyze: " + prevDegree + " " + currentDegree + " " + nextDegree);

               bool leftProb = false;
               bool rightProb = false;

               if (currentDegree - prevDegree < 2.0f)
               {
                  leftProb = true;
               }
               if (nextDegree - currentDegree < 2.0f)
               {
                  rightProb = true;
               }

               if (leftProb == false && rightProb == false)
               {
                  print("no problem!");
                  noProblemCount++;
               }
               if (leftProb == true && rightProb == false) //move right
               {
                  print("trying to move right");
                  float potentialDegree = currentDegree + 2.0f;
                  if (potentialDegree < nextDegree)
                     p.degree = potentialDegree;
                  else
                     print("  no room to move right");
               }
               if (leftProb == false && rightProb == true) //move left
               {
                  print("trying to move left");
                  float potentialDegree = currentDegree - 2.0f;
                  if (potentialDegree > prevDegree)
                     p.degree = potentialDegree;
                  else
                     print("no room to move left");
               }
               if (leftProb == true && rightProb == true)
                  print("problems on both sides, can't do anything!");
            }
            if (noProblemCount == ourList.Count)
            {
               print("Found no problems on iteration: " + e);
               break;
            }
         }
         print("--------------------------------------------");
         foreach (PlaceMark p in ourList)
         {
            print("after adjustment: " + p.gameObject.name + " degrees: " + p.degree);
            p.ApplyDegrees();
         }
         print("--------------------------------------------");
      }
      */
   }
}

