﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Valve.VR;

using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration.Data;
using Mapbox.Utils;
using Mapbox.Unity.Utilities;

public class ItemSelection : MonoBehaviour
{
   [HideInInspector]
   public GameObject RightHand;

   public GameObject globe; //TODO - this is polluting inspector with too many variables
   public GameObject zoomMap;
   public GameObject tableMap;
   public GameObject tableMap2;
   public LoadPlacesJSON ourPlacesLoader;
   public GameObject leftPanel;
   public GameObject rightPanel;
   public GameObject ourExcavationToggle;
   public GameObject ourAttributionToggle;
   public GameObject ourSchematicMaps;

   public GameObject selectionSphere;
   public DrawBoundsOnMap tableMapBoundsUpdater;
   public DrawBoundsOnMap rightwallMapBoundsUpdater;

   bool doVR;

   Vector3 prevHandForward;
   private bool previousTrigger;

   public GameObject wand;

   public float defaultWandLength;
   public float defaultWandThickness;

   private int layerMaskGlobe;
   private int terrainMask;
   private int layerMask;
   private int placeMask;
   private int wallMask;
   private int tableBookMask;
   private int searchMask;
   private int schematicMask;
   private int hotspotMask;
   private int excavationMask;
   private int attributionMask;

   public bool showOverview = true;

   private static int noInteraction = 0; //TODO: could be fancy and do this with an enum?
   private static int globeInteraction = 11;
   private static int terrainInteraction = 12;
   private static int placeInteraction = 13;
   private static int wallInteraction = 15;
   private static int tableBookInteraction = 16;
   private static int searchTermInteraction = 17;
   private static int schematicMapInteraction = 20;
   private static int hotSpotInteraction = 21;
   private static int excavationToggle = 22;
   private static int attributionToggle = 23;


   private int interactionType;
   public bool isSetup = false;
   GameObject ourMenuText;
   GameObject rightCamera;
   BuildType ourBuildType;

   // Use this for initialization
   void Start()
   {
      ourBuildType = GetComponent<WaitForMapsToBeReady>().ourBuildType;
      rightCamera = GetComponent<WaitForMapsToBeReady>().rightViewportCamera;

      if (ourBuildType == BuildType.WebGL)
         doVR = false;
      else
         doVR = true;

      interactionType = noInteraction;

      if (doVR)
      {
         //if (GameObject.Find("[SteamVR]")) //for HTC Vive
         //{
         //   doVR = true;
         //   RightHand = GameObject.Find("RightHand");
         //
         //}

         if (GameObject.Find("OVRCameraRig")) //for Oculus 
         {
            RightHand = GameObject.Find("RightHandAnchor");
         }
      }    

      layerMaskGlobe = 1 << globeInteraction;
      terrainMask = 1 << terrainInteraction;
      placeMask = 1 << placeInteraction;      
      wallMask = 1 << wallInteraction;
      tableBookMask = 1 << tableBookInteraction;
      searchMask = 1 << searchTermInteraction;
      schematicMask = 1 << schematicMapInteraction;
      hotspotMask = 1 << hotSpotInteraction;
      excavationMask = 1 << excavationToggle;
      attributionMask = 1 << attributionToggle;

      layerMask = layerMaskGlobe + terrainMask + placeMask + wallMask + tableBookMask + searchMask +  schematicMask + hotspotMask + excavationMask + attributionMask;

      ourMenuText = GameObject.Find("Menu Text");
      //QualitySettings.antiAliasing = 4;
   }

   void ShowRightMap()
   {
      print("hiding the overview terrain map now");
      tableMap.transform.parent.gameObject.SetActive(false);
      tableMap2.transform.parent.gameObject.SetActive(true);
      rightPanel.SetActive(false);

      tableMapBoundsUpdater.Hide(); //hide magenta square indicating range of zoom map
      showOverview = false;

      if (doVR)
      {
         Vector3 leftPos = leftPanel.transform.position;
         leftPos.y = 1.3f;
         leftPanel.transform.position = leftPos;

         Vector3 rightPos = rightPanel.transform.position;
         rightPos.y = 1.3f;
         rightPanel.transform.position = rightPos;
      }
   }

   void ShowOverviewText()
   {
      leftPanel.SetActive(true);
      rightPanel.SetActive(true);
      leftPanel.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = ourPlacesLoader.ourPlaces.wall_text_left;
      rightPanel.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = ourPlacesLoader.ourPlaces.wall_text_right;

      leftPanel.transform.Find("Image").gameObject.SetActive(false); //turn off images for now

      SchematicMapManager.SetupImage(ourPlacesLoader.ourPlaces.wall_image_right, 
                                     ourPlacesLoader.ourPlaces.wall_image_right_aspect_ratio, 
                                     rightPanel.transform.Find("Image").gameObject);

      if(doVR)
      {
         Vector3 leftPos = leftPanel.transform.position;
         leftPos.y = 1.8f;
         leftPanel.transform.position = leftPos;

         Vector3 rightPos = rightPanel.transform.position;
         rightPos.y = 1.8f;
         rightPanel.transform.position = rightPos;
      }
   }

   void ShowOverview(bool resetZoomLocation)
   {
      ShowOverviewText();

      if(showOverview==true)
      {
         print("already seeing overview");
         print("skipping reset");
         return;
      }

	  globe.SetActive(false); //don't show globe (which is the back button) in overview mode 
       
      ourMenuText.GetComponentInChildren<TMPro.TextMeshPro>().text = ourPlacesLoader.ourPlaces.name;

      tableMap.transform.parent.gameObject.SetActive(true);
      tableMap2.transform.parent.gameObject.SetActive(false);
      rightwallMapBoundsUpdater.Hide();  //hide magenta square indicating range of zoom map

      AbstractMap ourMap = tableMap.GetComponentInChildren<AbstractMap>(); 

      zoomMap.SetActive(true);
      //leftPanel.SetActive(false);
      ourSchematicMaps.SetActive(false);

      if (resetZoomLocation)
      {
         zoomMap.GetComponentInChildren<AbstractMap>().UpdateMap(new Vector2d(29.844722, 31.250833),17);
         tableMapBoundsUpdater.ForceRefresh();
      }
      else
      {
         zoomMap.GetComponentInChildren<AbstractMap>().UpdateMap(17.0f);
         tableMapBoundsUpdater.ForceRefresh();
      }      

      if (PlaceMark.activePlaceMark != null)
         PlaceMark.activePlaceMark.GetComponent<SchematicMapManager>().TurnOff();
     
      ourPlacesLoader.TurnAllOn(); //turn back on places marks
      PlaceMark.UnSelect();             

      showOverview = true;
   }

   // Update is called once per frame
   void Update()
   {
      if (isSetup == false && GetComponent<WaitForMapsToBeReady>().isReady)
      {
         ShowOverview(true);
         isSetup = true;
      }

      if (isSetup == false)
         return;

      RaycastHit hit;
      Ray ray = new Ray(); //remove errors
      Ray ray2 = new Ray(); //remove errors

      bool triggerDown = false;
      bool triggerUp = false;

      if (doVR == true)
      {
         ray = new Ray(RightHand.transform.position, RightHand.transform.forward);
         bool currentTrigger = false;

         //if HTC Vive
         //triggerDown = SteamVR_Input.__actions_default_in_GrabPinch.GetStateDown(SteamVR_Input_Sources.RightHand);
         //triggerUp = SteamVR_Input.__actions_default_in_GrabPinch.GetStateUp(SteamVR_Input_Sources.RightHand);

         //if Oculus
         if (ourBuildType == BuildType.OculusGo)
         {
            currentTrigger = (OVRInput.Get(OVRInput.Button.PrimaryIndexTrigger) || OVRInput.Get(OVRInput.Button.SecondaryIndexTrigger));

         }

         if (ourBuildType == BuildType.Vive)
         {
            float val = Input.GetAxis("Oculus_CrossPlatform_SecondaryIndexTrigger");
            if (val == 1.0f)
               currentTrigger = true;
         }

         triggerDown = ((currentTrigger == true) && (previousTrigger == false));
         triggerUp = ((currentTrigger == false) && (previousTrigger == true));
         previousTrigger = currentTrigger;
      }
      else //for desktop mode
      {
         triggerDown = Input.GetButtonDown("Fire1");
         triggerUp = Input.GetButtonUp("Fire1");
         ray = Camera.main.ScreenPointToRay(Input.mousePosition);
         ray2 = rightCamera.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
      }

      GameObject touchedObject = null;



      wand.transform.localPosition = new Vector3(0, 0, defaultWandLength * 0.5f); //adjust wand to reach out to whatever we are hitting; //if not contacting any objects, return to regular length
      wand.transform.localScale = new Vector3(defaultWandThickness, defaultWandThickness, defaultWandLength);

      if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
      {
         float distance = Vector3.Distance(hit.point, wand.transform.parent.position);

         Vector3 newPos = new Vector3(0, 0, distance * 0.5f); //adjust wand to reach out to whatever we are hitting
         Vector3 newScale = new Vector3(defaultWandThickness, defaultWandThickness, distance);

         wand.transform.localPosition = newPos;
         wand.transform.localScale = newScale;

         touchedObject = hit.collider.gameObject;
      }
      else if (doVR == false) //test other viewport
      {
         if(Physics.Raycast(ray2, out hit, Mathf.Infinity, layerMask))
         {
            float distance = Vector3.Distance(hit.point, wand.transform.parent.position); //TODO: don't just copy paste from above

            Vector3 newPos = new Vector3(0, 0, distance * 0.5f); //adjust wand to reach out to whatever we are hitting
            Vector3 newScale = new Vector3(defaultWandThickness, defaultWandThickness, distance);

            wand.transform.localPosition = newPos;
            wand.transform.localScale = newScale;

            touchedObject = hit.collider.gameObject;
         }
      }     

      /////////////////////////////
      /// PROCESS CLICKS
      ////////////////////////////
      if (touchedObject != null && (interactionType == noInteraction) && triggerDown)
      {
         print("user clicked");

         if (touchedObject.layer == excavationToggle)
         {
            print("user clicked excavation toggle for: " + touchedObject.name);
            touchedObject.GetComponent<ExcavationController>().Clicked();
         }
         if (touchedObject.layer == hotSpotInteraction)
         {
            print("user clicked a hotspot for: " + touchedObject.name);
            PlaceMark.activePlaceMark.GetComponent<SchematicMapManager>().HotspotClicked(touchedObject); 
         }
         if (touchedObject.layer == globeInteraction) //process globe dragging
         {
            print("user clicked globe");

            interactionType = globeInteraction;
            prevHandForward = ray.direction;

            //deltaMap.SetActive(true);
            //zoomMap.SetActive(false);

            ShowOverview(false); //TODO see what scene we are in
         }
         if(touchedObject.layer == schematicMapInteraction)
         {
            print("user clicked schematic map");

            interactionType = schematicMapInteraction;
            prevHandForward = ray.direction;
         }
         if (touchedObject.layer == terrainInteraction) //process tablemap interactions
         {
            print("clicked table at: " + hit.point);
            interactionType = terrainInteraction;
            zoomMap.SetActive(true);        
         }
         if (touchedObject.layer == placeInteraction) //process place mark interactions
         {
            print("user clicked placemark");
            touchedObject.transform.parent.GetComponent<PlaceMark>().Clicked(this);

            if (PlaceMark.activePlaceMark.GetComponent<PlaceMark>().placeInfo.menu != "")
            {
			   globe.SetActive(true); //turn on globe (which is the back button)
               ourSchematicMaps.SetActive(true); //only jump to schematic map mode if we have the additional data file specified
               ShowRightMap();
            }
         }      
         if(touchedObject.layer == attributionToggle)
         {
            touchedObject.GetComponent<AttributionToggle>().Clicked();
         }
      }

      /////////////////////////
      /// PROCESS TOUCHES
      /////////////////////////
      if (touchedObject != null && interactionType == noInteraction)
      {
         if(touchedObject.layer == excavationToggle)       touchedObject.GetComponent<ExcavationController>().Touched(touchedObject);
         if (touchedObject.layer == hotSpotInteraction)    PlaceMark.activePlaceMark.GetComponent<SchematicMapManager>().HotspotTouched(touchedObject);
         if (touchedObject.layer == placeInteraction)      touchedObject.transform.parent.GetComponent<PlaceMark>().Touched();
         if (touchedObject.layer == attributionToggle)     touchedObject.GetComponent<AttributionToggle>().Touched();

         //TODO: this is ineffecient. We shouldn't be turning everything off every frame!
         if (touchedObject.layer != excavationToggle)        ourExcavationToggle.GetComponent<ExcavationController>().Untouched();
         if (touchedObject.layer != placeInteraction)        PlaceMark.UnTouched();
         if (touchedObject.layer != attributionToggle)       ourAttributionToggle.GetComponentInChildren<AttributionToggle>().Untouched();

         if (touchedObject.layer != hotSpotInteraction)
         {
            if (PlaceMark.activePlaceMark != null)
               PlaceMark.activePlaceMark.GetComponent<SchematicMapManager>().HotspotUnTouched();
         }
      }
      if (touchedObject == null) //turn off if not touching anything
      {
         PlaceMark.UnTouched();
         ourAttributionToggle.GetComponentInChildren<AttributionToggle>().Untouched();
         ourExcavationToggle.GetComponent<ExcavationController>().Untouched();

         if (PlaceMark.activePlaceMark != null)
         {
            PlaceMark.activePlaceMark.GetComponent<SchematicMapManager>().HotspotUnTouched();
         }
      }

      //////////////////////////////////////
      /// PROCESS ONGOING INTERACTIONS
      //////////////////////////////////////

      if (interactionType == globeInteraction) //dragging globe processing. use just rotation for globe manipulation (as that's all we get with Oculus Go)
      {
         Vector3 handForward = ray.direction;
         float angle = Vector3.SignedAngle(prevHandForward, handForward, Vector3.up);
         globe.transform.rotation = globe.transform.rotation * Quaternion.Euler(0, angle * -2f, 0);
         prevHandForward = ray.direction;

         if (triggerUp)
            interactionType = noInteraction;
      }

      if (interactionType == schematicMapInteraction) //dragging globe processing. use just rotation for globe manipulation (as that's all we get with Oculus Go)
      {
         Vector3 handForward = ray.direction;
         float angle = Vector3.SignedAngle(prevHandForward, handForward, Vector3.up);
         ourSchematicMaps.transform.rotation = ourSchematicMaps.transform.rotation * Quaternion.Euler(0, angle * -2f, 0);
         prevHandForward = ray.direction;

         if (triggerUp)
            interactionType = noInteraction;
      }

      if (interactionType == terrainInteraction)
      {
         print("terrain interaction update!");
         if (touchedObject != null)
         {
            GameObject m;
            if (showOverview == true)
            {
               print("Using table map");
               m = tableMap;
            }
            else
            {
               m = tableMap2;
               print("using wall map");
            }

            AbstractMap _map = m.GetComponent<AbstractMap>();
            if (_map == null)
            {
               Debug.LogError("couldn't find attached map!");
               return;
            }

            selectionSphere.transform.parent = m.transform; //use a helper sphere to get the position in the coordinate system of the map
            selectionSphere.transform.position = hit.point;
            Vector3 unityCoord = selectionSphere.transform.localPosition * tableMap.transform.localScale.x; //weird mapbox bug? need to take into account scale of transform of abstractmap gameobject
            Vector2d pos_as_latlong = unityCoord.GetGeoPosition(_map.CenterMercator, _map.WorldRelativeScale);
            //print("lat/lon: " + pos_as_latlong);
            
            zoomMap.GetComponentInChildren<AbstractMap>().UpdateMap(pos_as_latlong);

            if (showOverview)
               tableMapBoundsUpdater.ForceRefresh(); //this will turn on outline on table map 
            else
               rightwallMapBoundsUpdater.ForceRefresh();

         }
         if (touchedObject == null)  //turn off terrain interaction if we drag off the map
            interactionType = noInteraction;
         if (triggerUp)           //turn off terrain interaction if we release the trigger
            interactionType = noInteraction;
      }
   }
}
