﻿using UnityEngine;
using Mapbox.Unity.MeshGeneration.Factories;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.MeshGeneration.Factories.TerrainStrategies;
using Mapbox.Unity.Map;
using Mapbox.Unity.Map.TileProviders;

public class SetupLinesOnGlobe : MonoBehaviour {
    public AbstractMap _map;
    public GameObject globeParent;

    public Mapbox.Utils.Vector2d[] locations;
    public GameObject lineObject;


    bool setup = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (setup==false && GetComponent<WaitForMapsToBeReady>().isReady)
        {
            int index = 0;
            var earthRadius = ((IGlobeTerrainLayer)_map.Terrain).EarthRadius;

            for (int i=0;i< locations.Length; i++)
            {             
                Vector3 pos = Conversions.GeoToWorldGlobePosition(locations[i], earthRadius*1.0005f); 
                Vector3 pos2 = Conversions.GeoToWorldGlobePosition(locations[(i+1)%locations.Length], earthRadius * 1.0005f);
                //print("pos: " + pos + " pos2: " + pos2);
              
                Vector3 centerPoint = (pos + pos2) * 0.5f;

                //GameObject line=GameObject.CreatePrimitive(PrimitiveType.Capsule);
                GameObject line = GameObject.Instantiate(lineObject);
                line.name = "segment_" + i.ToString();

                line.SetActive(true);
                line.transform.parent = globeParent.transform; //parent it under the globe
                line.transform.localPosition = centerPoint;

                float scale= (pos2 - pos).magnitude * 0.50f;
                line.transform.localScale = new Vector3(0.5f, 0.5f, scale);
                line.transform.localRotation = Quaternion.LookRotation((pos2 - pos).normalized);
                
                index++;
            }     

            setup = true;
        }        
    }
}


