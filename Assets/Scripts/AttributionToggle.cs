﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributionToggle : MonoBehaviour
{
   bool isShown = false;
   public GameObject attributionPanel;

   // Use this for initialization
   void Start()
   {
      Refresh();
   }

   void Refresh()
   {
      if (isShown)
      {
         attributionPanel.SetActive(true);
      }
      else
      {
         attributionPanel.SetActive(false);
      }
   }

   public void TurnOff()
   {
      isShown = false;
      Refresh();
   }

   public void Clicked()
   {
      if (isShown) isShown = false;
      else isShown = true;

      Refresh();
   }

   public void Touched()
   {
      this.GetComponent<Image>().enabled = true;
   }

   public void Untouched()
   {
      this.GetComponent<Image>().enabled = false;
   }
}
