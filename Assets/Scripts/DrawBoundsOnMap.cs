﻿using UnityEngine;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;

public class DrawBoundsOnMap : MonoBehaviour
{
    public GameObject ourMapGameObject;
    AbstractMap _targetmap;

    public GameObject targetMapParent;
    public GameObject linePrototypeObject;
    public float lineScale;

    private bool isSetup = false;
    public Vector2d[] pos_as_latlong;
    public GameObject[] lines;
    GetMapBounds gmb;

    private void Start()
    {
        _targetmap = targetMapParent.GetComponentInChildren<AbstractMap>();
        if (_targetmap == null)
        {
            Debug.LogError("No map is a child of the globe parent. did you delete it?");
            return;
        }
    }

    void Update()
    {
        if (isSetup == false && GetComponent<ItemSelection>().isSetup)
        {
            print("Trying to calc bounds for: " + ourMapGameObject.name);
            gmb = new GetMapBounds(ourMapGameObject);
            pos_as_latlong = gmb.CalcBounds();

            CreateLines();
            UpdateLines();
         isSetup = true;
        }

        /*if (setup)
        {
            foreach(Vector2d v in pos_as_latlong)
            {
                Vector3 pos=_targetmap.GeoToWorldPosition(v, true);
                Debug.DrawLine(pos - new Vector3(0, 0, 0.01f), pos + new Vector3(0, 0, 0.01f), Color.white);
                Debug.DrawLine(pos - new Vector3(0.01f, 0, 0f), pos + new Vector3(0.01f, 0, 0f), Color.white);
            }
        }*/
    }

     public void Hide()
   {
      foreach (GameObject g in lines)
         g.SetActive(false);
   }


    public void ForceRefresh()
    {
        foreach (GameObject g in lines)
           g.SetActive(true);


        if(isSetup == false)
        {
            return;
        }

        pos_as_latlong = gmb.CalcBounds();
        UpdateLines();
    }

    void CreateLines()
    {
        lines = new GameObject[4];

        for (int i = 0; i < 4; i++)
        {
            GameObject line = GameObject.Instantiate(linePrototypeObject);
            line.name = "segment_" + i.ToString();
            lines[i] = line;
        }
    }

    void UpdateLines() //TODO: consider using raycast (as we did in PlaceFunctionality) to get better y positioning
    {
        for (int i = 0; i < pos_as_latlong.Length; i++)
        {
            Vector3 pos = _targetmap.GeoToWorldPosition(pos_as_latlong[i], true);
            Vector3 pos2 = _targetmap.GeoToWorldPosition(pos_as_latlong[(i + 1) % pos_as_latlong.Length], true);
            Vector3 centerPoint = (pos + pos2) * 0.5f;

            GameObject line = lines[i];

            line.transform.parent = null;
            line.transform.position = centerPoint;
            float scaledLength = (pos2 - pos).magnitude * 1f; //change to 0.5f for capsule
            line.transform.localScale = new Vector3(lineScale, lineScale, scaledLength); 
            line.transform.rotation = Quaternion.LookRotation((pos2 - pos).normalized);
            line.transform.parent = targetMapParent.transform; //parent it under the targetmap

            line.SetActive(true);
        }

    }
}

