﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HourGlassController : MonoBehaviour
{
   bool isPresent;
   // Use this for initialization

   void Start() {
      SetPresent();
   }

   void SetPresent()
   {
      isPresent = true;
      GetComponentInChildren<TextMesh>().text = "Present";
   }

   void SetPast()
   {
      isPresent = false;
      GetComponentInChildren<TextMesh>().text = "450 BC";
   }

   public void Touched()
   {
      //GetComponent<Renderer>().material.color = Color.magenta; //not sure we need color change on hover
   }

   public void UnTouched()
   {
      //GetComponent<Renderer>().material.color = Color.white;
   }

   public void Clicked()
   {
      if (isPresent)
         SetPast();
      else
         SetPresent();
   }


	// Update is called once per frame
	void Update () {
		
	}
}
