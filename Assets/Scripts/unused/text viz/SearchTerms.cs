﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchResults
{
   public int book;
   public int chapter;
   public int line;
   public int pos;

   public float linePercent;
   public float posPercent;

   public SearchResults(int b, int c, int l, int p, AllBooks ab)
   {
      book = b;
      chapter = c;
      line = l;
      pos = p;

      linePercent = (float)line / (float)ab.books[b].chapters[c].lines.Count;

      int maxLineLength = 0;
      for(int i=0;i<ab.books[b].chapters[c].lines.Count;i++) //find the max length of a line in this chapter
      {
         string line = ab.books[b].chapters[c].lines[i];
         maxLineLength = Mathf.Max(maxLineLength, line.Length);
      }

      posPercent = Mathf.Clamp((float)pos / (float)maxLineLength, 0.0f,1.0f);

      Debug.Log("Pos percent: " + posPercent + " linePercent: " + linePercent);

   }

   public void Dump()
   {
      Debug.Log("Book: " + (book+1) + " chapter: " + (chapter+1) + " line: " + (line+1) + " position: " + (pos+1));
   }
}

public class SearchTerms : MonoBehaviour {

   public TableBooks ourTableBooks; //link to activate functions in table books

   bool isSetup = false;
   GameObject previousTermSelected = null;
   GameObject previousTermTouched = null;
   public GameObject searchTermPrototype;
   GameObject[] physicalSearchTerms;
   string[] searchTermsText = { "Memphis", "Egypt", "Proteus"};

   void Setup()
   {

      physicalSearchTerms = new GameObject[searchTermsText.Length];

      for (int i = 0; i < searchTermsText.Length; i++)
      {
         GameObject physicalSearchTerm = Instantiate(searchTermPrototype);
         physicalSearchTerms[i] = physicalSearchTerm; //save for later

         physicalSearchTerm.transform.Translate(0.8f * i, 0f, 0f);
         physicalSearchTerm.SetActive(true);
         physicalSearchTerm.name = i.ToString();
         physicalSearchTerm.GetComponentInChildren<TextMesh>().text = searchTermsText[i];
      }
   }

   // Update is called once per frame
   void Update()
   {
      if (isSetup == false)
      {
         Setup();
         isSetup = true;
      }
   }

   public List<SearchResults> PerformSearch(string term)
   {
      List<SearchResults> ourResults = new List<SearchResults>();

      AllBooks ourBooks = ourTableBooks.englishBooks.ourBooks;
      for (int b=0;b<ourBooks.books.Count;b++)
      {
         SingleBook sb = ourBooks.books[b];
         for (int c = 0; c < sb.chapters.Count; c++)
         {
            SingleChapter sc = sb.chapters[c];
            for (int l = 0; l < sc.lines.Count; l++)
            {
               string ourLine = sc.lines[l];
               int searchResult = 0;
               int stringpos = 0;

               while (searchResult != -1) //support multiple terms per line
               {
                  searchResult = ourLine.IndexOf(term, stringpos, StringComparison.CurrentCultureIgnoreCase);

                  if(searchResult!=-1)
                  {
                     ourResults.Add(new SearchResults(b, c, l, searchResult, ourBooks));
                     stringpos = searchResult + term.Length;
                  }                 
               }
            }
         }
      }
      print("term: " + term + " number of times found: " + ourResults.Count);
      foreach(SearchResults sr in ourResults)
      {
         sr.Dump();
      }

      return ourResults;
   }

   public void HandleMenuClick(GameObject clickedObject) //when being called externally (from ItemSelection.cs)
   {
      int bookInt = int.Parse(clickedObject.name);

      if (previousTermSelected == clickedObject)
      {
         clickedObject.GetComponentInChildren<TextMesh>().color = Color.black;
         previousTermSelected = null;
         //TODO: turn off terms here

         return;
      }

      if (previousTermSelected != null)
      {
         previousTermSelected.GetComponent<Renderer>().material.color = Color.white;
         previousTermSelected.GetComponentInChildren<TextMesh>().color = Color.black;
      }

      clickedObject.GetComponent<Renderer>().material.color = Color.magenta; //make text purple to indicate selection
      clickedObject.GetComponentInChildren<TextMesh>().color = Color.magenta;

      //TODO: update floating map here

      List<SearchResults> ourResults = PerformSearch(searchTermsText[bookInt]);
      ourTableBooks.ActivateSearchTerm(ourResults);

      previousTermSelected = clickedObject;
   }

   public void TurnOnTouched(GameObject touchedObject) //when being called externally (from ItemSelection.cs)
   {

      if (previousTermTouched != null)
         previousTermTouched.GetComponent<Renderer>().material.color = Color.white;


      touchedObject.GetComponent<Renderer>().material.color = Color.magenta;

      previousTermTouched = touchedObject;
   }

   public void TurnOffTouched()
   {
      if (previousTermTouched != null)
         previousTermTouched.GetComponent<Renderer>().material.color = Color.white;

      previousTermTouched = null;
   }
}
