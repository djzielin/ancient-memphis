﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;

public class ProcessText : MonoBehaviour
{
   string LoadTextAsset(string filename)
   {
      //TODO verify no extension as that will cause it fail!
      TextAsset txt = Resources.Load<TextAsset>(filename); 
      if (txt == null)
      {
         Debug.LogError("couldn't load text file!");
         return "";
      }
      return txt.text;
   }


   // Use this for initialization
   void Start()
   {

      print("processing histories english");

      print("  loading from disk...");  string allEnglish = LoadTextAsset("histories1_english") + "\r\n" + LoadTextAsset("histories2_english");
      print("  parsing...");            AllBooks abEnglish=LoadBooks(allEnglish);
      print("  converting to json..."); string jsonEnglish = JsonUtility.ToJson(abEnglish);
      print("  dumping to disk...");    File.WriteAllText("Assets/Resources/HistoriesEnglish.json", jsonEnglish, System.Text.Encoding.UTF8);
      print("  Done!");

      print("processing histories greek");

      print("  loading from disk...");  string allGreek = LoadTextAsset("histories1_greek") + "\r\n" + LoadTextAsset("histories2_greek");
      print("  parsing...");            AllBooks abGreek = LoadBooks(allGreek);
      print("  converting to json..."); string jsonGreek = JsonUtility.ToJson(abGreek);
      print("  dumping to disk...");    File.WriteAllText("Assets/Resources/HistoriesGreek.json", jsonGreek, System.Text.Encoding.UTF8);

      /*int book = 1;
      foreach(SingleBook b in ab.books)
      {
         print("Book: " + book + " number of chapters: " + b.chapters.Count);

         int chapters = 1;
         foreach (SingleChapter c in b.chapters)
         {
            print("  Chapter: " + chapters + " lines: " + c.lines.Count);
            chapters++;
         }
         book++;

      }*/


   }

   AllBooks LoadBooks(string text)
   {
      AllBooks ab = new AllBooks();

      // File is format
      //
      // BOOK I         //book begining
      // 1.             //chapter begining
      // 
      // 2. 
      // 
      // NOTES TO BOOK  //end of chapter
      // 
      // BOOK II
      // 1. 
      // 
      // 2.
      // 
      // (ect.)

      text = text.Replace("\u0097", ""); //get rid of "protected area code character"

      string[] bookNamesEnglish = { "BOOK I", "BOOK II", "BOOK III", "BOOK IV", "BOOK V", "BOOK VI", "BOOK VII", "BOOK VIII", "BOOK IX" };
      string[] bookNamesGreek = { "ΒΙΒΛΙΟΝ ΠΡΩΤΟΝ", "ΒΙΒΛΙΟΝ ΔΕΥΤΕΡΟΝ", "ΒΙΒΛΙΟΝ ΤΡΙΤΟΝ", "ΒΙΒΛΙΟΝ TΕTAPTOΝ", "ΒΙΒΛΙΟΝ ΠΕΜΠΤΟΝ", "ΒΙΒΛΙΟΝ ΕΚΤΟΝ", "ΒΙΒΛΙΟΝ ΕΒΔΟΜΟΝ", "ΒΙΒΛΙΟΝ ΟΓΔΟΟΝ", "ΒΙΒΛΙΟΝ ΕΝΝΑΤΟΝ" };

      //string[] bookNames = bookNamesGreek;
      string[] separatingChars = { "\r\n" }; 
      string[] allLines = text.Split(separatingChars, System.StringSplitOptions.None);

      int fileLineNumber = 0;

      for (int book = 1; book < bookNamesEnglish.Length + 1; book++) 
      {
         SingleBook sb = new SingleBook();
         ab.books.Add(sb);

         //print("Begining parsing for Book: " + book);
         bool foundBook = false;

         for (int i = fileLineNumber; i < allLines.Length; i++) //find the begining of the chapter
         {
            string line = allLines[i];
            if (line.IndexOf(bookNamesEnglish[book-1]) == 0) //always starts first on the line
            {
               //Debug.Log("We found the start of book: " + book);
               fileLineNumber = i;
               foundBook = true;
               break;
            }
            if (line.IndexOf(bookNamesGreek[book - 1]) == 0) //always starts first on the line
            {
               //Debug.Log("We found the start of book: " + book);
               fileLineNumber = i;
               foundBook = true;
               break;
            }
         }

         if (foundBook == false)
         {
            Debug.LogError("unable to find book: " + book);
            return ab;
         }


         for (int chap = 1; ; chap++) 
         {
            //print("Begining parsing for Chapter: " + chap);

            SingleChapter sc = new SingleChapter();
            sb.chapters.Add(sc);

            bool foundStart = false;
            bool missingChapter = false;
            bool bookDone = false;


            string startTermPeriod = chap.ToString() + ". ";
            string startTermComma = chap.ToString() + ", ";

            for (int i = fileLineNumber; i < allLines.Length; i++) //find the begining of the chapter
            {
               string line = allLines[i];

               if (line.IndexOf(startTermPeriod) == 0) //always starts first on the line
               {
                  fileLineNumber = i;
                  foundStart = true;
                  break;
               }
               if (line.IndexOf(startTermComma) == 0)
               {
                  fileLineNumber = i;
                  foundStart = true;
                  break;
               }
            }

            if (foundStart == false)
            {
               Debug.LogError("Error: Can't find start of chapter: " + chap + " in book: " + book);
               return ab;
            }

            //print("found start of chapter: " + chap + " on line number: " + fileLineNumber);

            string nextTermPeriod = (chap + 1).ToString() + ". ";
            string nextTermComma = (chap + 1).ToString() + ", ";

            string nextNextTermPeriod = (chap + 2).ToString() + ". ";
            string nextNextTermComma = (chap + 2).ToString() + ", ";

            for (int i = fileLineNumber; i < allLines.Length; i++)
            {
               string line = allLines[i];

               if (i == fileLineNumber) //remove the chapter begining. ie. pull off "1. "
               {
                  line = line.Substring(startTermPeriod.Length); 
               }               

               if (line.IndexOf("NOTES TO BOOK") == 0) //check for end of book
               {
                  //Debug.Log("Found notes section at line: " + fileLineNumber + " so ending chapter: " + chap);
                  bookDone = true;
                  fileLineNumber = i;
                  break;
               }

               if (line.IndexOf("Τ Ε Λ Ο Σ") == 0 || line.IndexOf("ΤΕΛΟΣ ΤΟΥ Α") == 0) //check for end of book
               {
                  //Debug.Log("Found greek section tag at line: " + fileLineNumber + " so ending chapter: " + chap);
                  bookDone = true;
                  fileLineNumber = i;
                  break;
               }

               if (book < bookNamesGreek.Length) //check for end of book
               {
                  if (line.IndexOf(bookNamesGreek[book]) == 0) 
                  {
                     //Debug.Log("Found the next book title: " + fileLineNumber + " so ending chapter: " + chap);
                     bookDone = true;
                     fileLineNumber = i;
                     break;
                  }
               }

               if (line.IndexOf(nextTermPeriod) == 0 || line.IndexOf(nextTermComma) == 0) //check for end of chapter
               {
                  //Debug.Log("Found the next section, so ending chapter: " + chap);
                  fileLineNumber = i;
                  break;
               }

               if (book==6 && chap == 139 && line.IndexOf("140 Ταύτα") == 0 ) //special case. 140 is missing period! 
               {
                  //Debug.Log("Found the next section, so ending chapter: " + chap);
                  fileLineNumber = i;
                  allLines[fileLineNumber]=allLines[fileLineNumber].Insert(3, ".");
                  break;
               }

               if (line.IndexOf(nextNextTermPeriod) == 0 || line.IndexOf(nextNextTermComma) == 0) //we sometimes have missing chapter. ie, book 1 chap 44 is missing
               {
                  //Debug.Log("Found the next next section, so ending chapter: " + chap);

                  missingChapter = true;
                  fileLineNumber = i;
                  break;
               }

               line = Regex.Replace(line, @"[\d]", string.Empty); //remove note references (appear as numbers), hopefully this is okay? ie, numbers don't appear in text ever?
               line = line.Replace("  ", " "); //replace double space with single
               sc.lines.Add(line);
            }

            while (sc.lines[sc.lines.Count - 1] == "") //cleanup. pull off blank endlines
            {
               sc.lines.RemoveAt(sc.lines.Count - 1);
            }

            int lIndex = 1;
            foreach (string s in sc.lines)
            {
               //print("Book: " + book + " Chapter: " + chap + " Line: " + lIndex + " Text: " + s);
               lIndex++;
            }

            if (missingChapter)
            {
               SingleChapter scMissing = new SingleChapter(); //add in a blank chapter
               sb.chapters.Add(scMissing);
               chap++;
            }

            if (bookDone)
               break;
         }
      }
      return ab;
   }





   // Update is called once per frame
   void Update()
   {


   }
}
