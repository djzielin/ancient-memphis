﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableBooks : MonoBehaviour {
   public LoadTextJSON englishBooks;

   bool isSetup = false;
   GameObject previousBookSelected = null;
   GameObject previousBookTouched = null;
   public GameObject bookPrototype;
   public GameObject searchTermCubePrototype;
   public GameObject floatingBookMap;

   GameObject[] physicalBooks;
   GameObject[] physicalTermLocations;
   List<SearchResults> ourResults;

   // Use this for initialization
   void Start () {
		
	}

   void Setup()
   {
      string[] romanNames = { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };
      physicalBooks = new GameObject[englishBooks.ourBooks.books.Count];

      for (int i = 0; i < englishBooks.ourBooks.books.Count; i++)
      {
         GameObject physicalBook = Instantiate(bookPrototype);
         physicalBooks[i] = physicalBook; //save for later

         physicalBook.transform.Translate(0.15f * i, 0f, 0f);
         physicalBook.SetActive(true);
         physicalBook.name = i.ToString();
         physicalBook.GetComponentInChildren<TextMesh>().text = romanNames[i];
      }
   }

   // Update is called once per frame
   void Update ()
   {
	   if(isSetup==false)
      {
         Setup();
         isSetup = true;
      }      
	}

   public void HandleMenuClick(GameObject clickedObject) //when being called externally (from ItemSelection.cs)
   {
      int bookInt = int.Parse(clickedObject.name);

      if(previousBookSelected==clickedObject)
      {
         //must be tryign to turn off

         clickedObject.GetComponentInChildren<TextMesh>().color = Color.black;
         previousBookSelected = null;

         //TODO turn off floating map here
         floatingBookMap.SetActive(false);
         return;
      }

      if (previousBookSelected != null)
      {
         previousBookSelected.GetComponent<Renderer>().material.color = Color.white;
         previousBookSelected.GetComponentInChildren<TextMesh>().color = Color.black;
      }

      clickedObject.GetComponent<Renderer>().material.color = Color.magenta; //make text purple to indicate selection
      clickedObject.GetComponentInChildren<TextMesh>().color = Color.magenta;

      //TODO: update floating map here

      floatingBookMap.SetActive(true);
      //floatingBookMap.GetComponent<FloatingBookController>().DisplaySearchResults(bookInt,ourResults);

      previousBookSelected = clickedObject;
   }

   public void TurnOnTouched(GameObject touchedObject) //when being called externally (from ItemSelection.cs)
   {

      if (previousBookTouched != null)
         previousBookTouched.GetComponent<Renderer>().material.color = Color.white;


      touchedObject.GetComponent<Renderer>().material.color = Color.magenta;

      previousBookTouched = touchedObject;
   }

   public void TurnOffTouched()
   {
      if (previousBookTouched != null)
         previousBookTouched.GetComponent<Renderer>().material.color = Color.white;

      previousBookTouched = null;
   }

   void RemoveTerms()
   {
      if (physicalTermLocations == null)
         return;

      foreach (GameObject g in physicalTermLocations)
         Destroy(g);

      physicalTermLocations = null;
   }

   public void ActivateSearchTerm(List<SearchResults> results)
   {
      RemoveTerms();

      physicalTermLocations = new GameObject[results.Count];

      for (int i=0;i<results.Count;i++)
      {
         GameObject cube = Instantiate(searchTermCubePrototype);
         physicalTermLocations[i] = cube; //store for later
         cube.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

         int b = results[i].book;
         int c = results[i].chapter;
         int l = results[i].line;

         int cmax = englishBooks.ourBooks.books[b].chapters.Count;
         int dimensions = (int)Mathf.Ceil(Mathf.Sqrt((float)cmax))+2; //give buffer                 
         
         Vector3 bookCenter=physicalBooks[b].GetComponent<Renderer>().bounds.center;
         Vector3 bookExtents=physicalBooks[b].GetComponent<Renderer>().bounds.extents;
         Vector3 upperLeft = bookCenter + new Vector3(-bookExtents.x, bookExtents.y, bookExtents.z);

         int column = (c % dimensions) + 1 ; //give one cell buffer
         int row = ((c - column) / dimensions) + 1;

         float chapterwidth = (bookExtents.x * 2.0f) / (float)(dimensions +2);
         float chapterheight = (bookExtents.z * 2.0f) / (float)(dimensions +2);

         float locationX = upperLeft.x + column * chapterwidth + results[i].posPercent * chapterwidth;
         float locationY = upperLeft.y;
         float locationZ = upperLeft.z - (row * chapterheight + results[i].linePercent * chapterheight);

         cube.transform.position = new Vector3(locationX, locationY, locationZ);

         cube.SetActive(true);

      }
      ourResults = results;
   }

}
