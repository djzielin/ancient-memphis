﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AllBooks
{
   public List<SingleBook> books = new List<SingleBook>();
}

[System.Serializable]
public class SingleBook
{
   public List<SingleChapter> chapters = new List<SingleChapter>();
}

[System.Serializable]
public class SingleChapter
{
   public List<string> lines = new List<string>();
}

public class LoadTextJSON : MonoBehaviour {
   public string fileName;
   public AllBooks ourBooks;
	// Use this for initialization
	void Start () {
      TextAsset txt = Resources.Load<TextAsset>(fileName); //ommit .json extension
      if (txt == null)
      {
         Debug.LogError("couldn't load text file: " + fileName);
         return;
      }
      string filecontent = txt.text;
      ourBooks = JsonUtility.FromJson<AllBooks>(filecontent);
      print("Succesfully Loaded: " + fileName);
   }

}
