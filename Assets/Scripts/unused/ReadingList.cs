﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadingList : MonoBehaviour {
    GameObject ourCanvas;
    public UpdateBook ourBookUpdater;

    GameObject previousTextSelected;
    GameObject previousTextTouched;

    GameObject[] ourLinks;


	// Use this for initialization
	void Start ()
    {
        ourCanvas=transform.Find("MenuCanvas").gameObject;
        if(ourCanvas==null)
        {
            Debug.LogError("no canvas found!");
        }
	}
	
    public void TurnOff()
    {
       DestroyLinks();
       ourCanvas.SetActive(false);
    }

    public void HandleMenuClick(GameObject clickedObject) //when being called externally (from ItemSelection.cs)
    {
       int menuInt = int.Parse(clickedObject.name);

        if (previousTextSelected != null)
            previousTextSelected.GetComponent<Renderer>().material.color = Color.blue;

        clickedObject.GetComponent<Renderer>().material.color = Color.magenta; //make text purple to indicate selection

        PlaceMark pmScript = PlaceMark.activePlaceMark.GetComponent<PlaceMark>();
        //ourBookUpdater.SetBookText(pmScript.ourMenu.data[menuInt].leftPage, pmScript.ourMenu.data[menuInt].rightPage);

        previousTextSelected = clickedObject;
        
    }

    public void TurnOnTouched(GameObject touchedObject) //when being called externally (from ItemSelection.cs)
    {
        PlaceMark pmScript = PlaceMark.activePlaceMark.GetComponent<PlaceMark>();

        if (previousTextTouched != null)
            previousTextTouched.GetComponent<Renderer>().material.color = Color.blue;


        touchedObject.GetComponent<Renderer>().material.color = Color.black;

        previousTextTouched = touchedObject;
    }

    public void TurnOffTouched()
    {
        if (previousTextTouched != null)
            previousTextTouched.GetComponent<Renderer>().material.color = Color.blue;
        
        previousTextTouched = null;
    }

    IEnumerator SetupUI()
    {
      yield return 0;
      /* PlaceMark pmScript = PlaceMark.activePlaceMark.GetComponent<PlaceMark>();
       string description = ""; // pmScript.ourMenu.description;
       GameObject d = ourCanvas.transform.Find("Description").gameObject;
       Text t = d.GetComponent<Text>();

       yield return 0;

       ourLinks = new GameObject[pmScript.ourMenu.data.Count];

       for (int i = 0; i < pmScript.ourMenu.data.Count; i++)
       {
           string linkName = pmScript.ourMenu.data[i].name;
           print("trying to setup: " + linkName);

           int linkStartPos=t.text.IndexOf(linkName);
           if (linkStartPos == -1)
           {
               Debug.LogError("link not found in the description!");
               break;
           }
           int linkLength = linkName.Length;
           int linkEndPos = linkStartPos + linkLength-1;       

           float scaleFactor=ourCanvas.GetComponent<Canvas>().scaleFactor;

           RectTransform rt = t.rectTransform;

           Vector3 topLeft = (d.GetComponent<Text>().cachedTextGenerator.verts[linkStartPos * 4 + 0].position / scaleFactor);
           Vector3 botLeft = (d.GetComponent<Text>().cachedTextGenerator.verts[linkStartPos * 4 + 3].position / scaleFactor);
           Vector3 topRight = (d.GetComponent<Text>().cachedTextGenerator.verts[linkEndPos * 4 + 1].position / scaleFactor);
           Vector3 botRight = (d.GetComponent<Text>().cachedTextGenerator.verts[linkEndPos * 4 + 2].position / scaleFactor);

           float[] xVals = new float[4];
           xVals[0] = topLeft.x;
           xVals[1] = botLeft.x;
           xVals[2] = topRight.x;
           xVals[3] = botRight.x;

           float[] yVals = new float[4];
           yVals[0] = topLeft.y;
           yVals[1] = botLeft.y;
           yVals[2] = topRight.y;
           yVals[3] = botRight.y;

           float maxX = Mathf.Max(xVals);
           float minX = Mathf.Min(xVals);
           float maxY = Mathf.Max(yVals);
           float minY = Mathf.Min(yVals);              

           float centerX = (maxX + minX) * 0.5f; 
           float xSize = Mathf.Abs(maxX-minX);

           float centerY = (maxY + minY) * 0.5f; 
           float ySize = Mathf.Abs(maxY-minY);

           GameObject linkPrototype=transform.Find("Link Prototype").gameObject;
           GameObject link = Instantiate(linkPrototype);
           link.transform.parent = this.transform;
           link.transform.rotation = new Quaternion();
           link.name = i.ToString();


           link.transform.position = rt.position;
           Vector3 localPos = link.transform.localPosition;
           link.transform.localPosition=localPos + new Vector3(centerX * 0.001f, centerY * 0.001f, 0.008f);

           link.transform.localScale = new Vector3(xSize * 0.001f+0.01f, ySize * 0.001f+0.01f, 0.01f);

           link.GetComponent<Renderer>().material.color = Color.blue;
           link.SetActive(true);
           ourLinks[i] = link;
       }
       */
   }

    void DestroyLinks()
    {
        if (ourLinks != null)
        {
            foreach (GameObject g in ourLinks)
                Destroy(g);
            ourLinks = null;
        }
    }


    public void SetupAndTurnOn()
    {
        DestroyLinks();

        PlaceMark pmScript = PlaceMark.activePlaceMark.GetComponent<PlaceMark>();
        ourCanvas.SetActive(true);

        GameObject t = ourCanvas.transform.Find("Title").gameObject;
        t.GetComponent<Text>().text = pmScript.ourMenu.name;

        GameObject d = ourCanvas.transform.Find("Description").gameObject;
      string description = ""; // pmScript.ourMenu.description;
        d.GetComponent<Text>().text = description;

        //need to wait one frame here (get text laid out properly)
        StartCoroutine(SetupUI());            
        
        previousTextSelected = null;
        previousTextTouched = null;
    }
}
