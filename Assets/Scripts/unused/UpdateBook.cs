﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateBook : MonoBehaviour {
    public GameObject leftPageCamera; 
    public GameObject rightPageCamera;

    public GameObject leftPageText;
    public GameObject rightPageText;

    public void Start()
    {
        ClearBook();
    }


    public void SetBookText(string left, string right)
    {
        leftPageText.GetComponent<Text>().text = left;
        rightPageText.GetComponent<Text>().text = right;
        RefreshBook();
    }
    
    public void ClearBook()
    {
        leftPageText.GetComponent<Text>().text = "";
        rightPageText.GetComponent<Text>().text = "";
        RefreshBook();
    }

    public void RefreshBook()
    {
        print("refreshing book");
        StartCoroutine(toggleLeft());
        StartCoroutine(toggleRight());
    }

    private IEnumerator toggleLeft()
    {
        leftPageCamera.SetActive(true);
        yield return 0; //wait for one frame
        leftPageCamera.SetActive(false);
    }

    private IEnumerator toggleRight()
    {
        rightPageCamera.SetActive(true);
        yield return 0;
        rightPageCamera.SetActive(false);
    }
}
