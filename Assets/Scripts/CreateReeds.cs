﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(GameObject))]
public class ObjectBuilderEditor : EditorWindow
{
   //static public Material reedMaterial;

   [MenuItem("Tools/Dave/CreateReeds")]
   static void Execute()
   {
         GameObject objToSpawn = new GameObject("Prototype Reeds");
         CreateSimplePlane(objToSpawn);
         //objToSpawn.GetComponent<Renderer>().material = reedMaterial;
   }

   // modified from
   // https://answers.unity.com/questions/496202/self-coded-plane-vs-unity-plane.html

   static void CreateSimplePlane(GameObject g)
   {
      Mesh mesh = new Mesh();

      Vector3[] square = new Vector3[4];

      square[0] = new Vector3(-0.5f, 0.5f, 0);
      square[1] = new Vector3(0.5f, 0.5f, 0);
      square[2] = new Vector3(0.5f, -0.5f, 0);
      square[3] = new Vector3(-0.5f, -0.5f, 0);

      int[] tri = new int[6];

      tri[0] = 0;
      tri[1] = 1;
      tri[2] = 3;

      tri[3] = 2;
      tri[4] = 3;
      tri[5] = 1;

      Vector2[] uv = new Vector2[4];

      uv[0] = new Vector2(1, 1);
      uv[1] = new Vector2(0, 1);
      uv[2] = new Vector2(0, 0);
      uv[3] = new Vector2(1, 0);

      Vector3[] normals = new Vector3[4];

      normals[0] = -Vector3.forward;
      normals[1] = -Vector3.forward;
      normals[2] = -Vector3.forward;
      normals[3] = -Vector3.forward;

      mesh.vertices = square;
      mesh.triangles = tri;
      mesh.uv = uv;
      mesh.normals = normals;

      g.AddComponent<MeshRenderer>();
      g.AddComponent<MeshFilter>().mesh = mesh;
   }
}

#endif