﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplayFPS : MonoBehaviour {
    float timeAccumulated;
    float framesAccumulated;

    void resetCounters()
    {
        timeAccumulated = 0f;
        framesAccumulated = 0f;
    }

	// Use this for initialization
	void Start () {
        resetCounters();
	}


	
	// Update is called once per frame
	void Update () {
        timeAccumulated += Time.deltaTime;
        framesAccumulated += 1;

        if(timeAccumulated>1.0f)
        {
            float fps = framesAccumulated/timeAccumulated;
            GetComponent<TextMesh>().text = "FPS: " + fps;
            resetCounters();
        }
	}
}
