﻿using UnityEngine;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;

//look through all the tiles to figure out the lat/long extent of the map

public class DrawBoundsOnGlobe : MonoBehaviour
{
    public GameObject ourMapGameObject;
    public GameObject globeParent;
    public GameObject linePrototypeObject;
    public float lineScale;

    private bool setup = false;
    public  Vector2d[] pos_as_latlong;
  
    void Update()
    {
        if (setup == false && GetComponent<WaitForMapsToBeReady>().isReady)
        {
            print("Trying to calc bounds for: " + ourMapGameObject.name);
            GetMapBounds gmb = new GetMapBounds(ourMapGameObject);
            pos_as_latlong = gmb.CalcBounds();

            DrawLines();
            setup = true;
        }
    }

    void DrawLines()
    {
        AbstractMap _globemap = globeParent.GetComponentInChildren<AbstractMap>();
        if(_globemap==null)
        {
            Debug.LogError("No map is a child of the globe parent. did you delete it?");
            return;
        }

        int index = 0;
        var earthRadius = ((IGlobeTerrainLayer)_globemap.Terrain).EarthRadius;

        for (int i = 0; i < pos_as_latlong.Length; i++)
        {
            Vector3 pos = Conversions.GeoToWorldGlobePosition(pos_as_latlong[i], earthRadius * 1.0005f);
            Vector3 pos2 = Conversions.GeoToWorldGlobePosition(pos_as_latlong[(i + 1) % pos_as_latlong.Length], earthRadius * 1.0005f);

            Vector3 centerPoint = (pos + pos2) * 0.5f;

            //GameObject line=GameObject.CreatePrimitive(PrimitiveType.Capsule);
            GameObject line = GameObject.Instantiate(linePrototypeObject);
            line.name = "segment_" + i.ToString();

            line.SetActive(true);
            line.transform.parent = globeParent.transform; //parent it under the globe
            line.transform.localPosition = centerPoint;

            float scaledLength = (pos2 - pos).magnitude * 0.50f;
            line.transform.localScale = new Vector3(lineScale, lineScale, scaledLength);
            line.transform.localRotation = Quaternion.LookRotation((pos2 - pos).normalized);
        }

    }
}
