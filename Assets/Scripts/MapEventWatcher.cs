﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using Mapbox.Unity.Map.TileProviders;

public class MapEventWatcher : MonoBehaviour {
    bool rtp_setup = false;

	// Use this for initialization
	void Start ()
    {
        GetComponent<AbstractMap>().OnInitialized += MapInit;
        GetComponent<AbstractMap>().OnMapRedrawn += MapRedrawn;
        GetComponent<AbstractMap>().OnUpdated += MapUpdated;

        RangeTileProvider rtp = GetComponent<RangeTileProvider>();
        if (rtp != null && rtp_setup == false)
        {
            rtp.ExtentChanged += MapExtentChanged;

        }
    }

    void MapExtentChanged(object sender, ExtentArgs e)
    {
        print(this.gameObject.name + " Map extent changed at time: " + Time.time + " frame: " + Time.frameCount);
    }

    
    void MapInit()
    {
        print(this.gameObject.name + " Map Inited at time: " + Time.time + " frame: " + Time.frameCount);
        print(this.gameObject.name + "   has children count: " + this.gameObject.transform.childCount);

    }

    void MapUpdated()
    {
        print(this.gameObject.name + " Map Updated at time: " + Time.time + " frame: " + Time.frameCount);
    }

    void MapRedrawn()
    {
        print(this.gameObject.name + " Map Redrawn at time: " + Time.time + " frame: " + Time.frameCount);
    }

    // Update is called once per frame
    void Update () {
        RangeTileProvider rtp = GetComponent<RangeTileProvider>();
        if (rtp != null && rtp_setup == false)
        {
            rtp.ExtentChanged += MapExtentChanged;

        }
    }
}
