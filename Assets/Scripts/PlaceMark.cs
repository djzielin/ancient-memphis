﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine.UI;

[System.Serializable]
public class WholeMenu
{
   public string name;

   public List<SingleSchematic> schematic_maps = new List<SingleSchematic>();
}

[System.Serializable]
public class SingleSchematic
{
   public string name;
   public string wall_text;
   public string icon;
   public string model;
   public int vector_layer;
   public double lat;
   public double lon;

   public List<SingleHotSpot> hot_spots = new List<SingleHotSpot>();
}

[System.Serializable]
public class SingleHotSpot
{
   public string name;
   public double lat;
   public double lon;
   public string collider;
   public string image;
   public float aspect_ratio;

   public string hover_text;
   public string wall_text;
}

public class PlaceMark : MonoBehaviour
{
   public Vector3 targetPosition;

   public SinglePlace placeInfo; //from JSON files
   public WholeMenu ourMenu;
   public GameObject ourRightMap;
   public GameObject zoomMap;

   GameObject ourPointer;
   [HideInInspector]
   public GameObject ourIcon;
   GameObject ourText;

   public LoadPlacesJSON ourLoader;


   public static GameObject activePlaceMark;
   static GameObject touchedText = null;
   static GameObject touchedIcon = null;

   [HideInInspector]
   public float degree;
   [HideInInspector]
   public float magnitude;


   public static int SortByDegree(PlaceMark p1, PlaceMark p2)
   {
      return p1.degree.CompareTo(p2.degree);
   }

   // Use this for initialization
   void Awake()
   {
      ourPointer = transform.Find("Cone").gameObject;
      ourIcon = transform.Find("IconHolder").gameObject;
      ourText = transform.Find("Text").gameObject;
   }

   public void ApplyDegrees()
   {
      Vector3 pos = transform.position;

      float x = magnitude * Mathf.Cos(degree * Mathf.Deg2Rad) + Camera.main.transform.position.x;
      float y = pos.y;
      float z = magnitude * Mathf.Sin(degree * Mathf.Deg2Rad) + Camera.main.transform.position.z;

      transform.position = new Vector3(x, y, z);

      SetupPointer();

   }

   void DoBillboarding()
   {
      if (Camera.main == null) //cameras not setup yet
         return;

      Vector3 camPos = Camera.main.transform.position;

      Vector3 ourPos = ourIcon.transform.position;
      if (ourIcon.GetComponent<BoxCollider>() != null)
         ourPos = ourIcon.transform.TransformPoint(ourIcon.GetComponent<BoxCollider>().center); //get this as world space

      camPos.y = 0; //don't want to pitch text
      ourPos.y = 0;

      Vector3 direction = (ourPos - camPos).normalized;
      transform.rotation = Quaternion.LookRotation(direction, Vector3.up); //Question: should this change in orientation happening instantly or over time? Hmmmm
   }  


   // Update is called once per frame
   void Update()
   {
      DoBillboarding(); //TODO: don't do every frame?
      SetupPointer();
   }

   public static void UnTouched()
   {
      if (touchedText != null)
      {
         touchedText.SetActive(false);
         touchedText = null;
      }

      if (touchedIcon != null)
      {
         //touchedIcon.GetComponent<Renderer>().material.DisableKeyword("_EMISSION"); //get text back to black
         touchedIcon.GetComponent<Renderer>().material.color = Color.black;
         touchedIcon = null;
      }
   }

  public void Touched()
   {
      if (ourText != touchedText)
         UnTouched();

      touchedText = ourText;
      touchedText.SetActive(true);
      touchedText.GetComponent<TMPro.TextMeshPro>().color = Color.magenta;

      touchedIcon = ourIcon;
      //touchedIcon.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
      //touchedIcon.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.magenta);     
      touchedIcon.GetComponent<Renderer>().material.color = Color.magenta;
   }
     
   public void Clicked(ItemSelection isel)
   {
      if (ourText != touchedText)
         UnTouched();

      if (placeInfo.menu == "")
      {
         isel.zoomMap.SetActive(true);
         isel.zoomMap.GetComponentInChildren<AbstractMap>().UpdateMap(new Vector2d(placeInfo.lat, placeInfo.lon));
         isel.tableMapBoundsUpdater.ForceRefresh();
      }
      else //handle the switch over to city center mode. 
      {
         ourRightMap.SetActive(true);
         if (ourRightMap == null)
            Debug.LogError("ourRightMap not defined!");

         print("our Map name is: " + ourRightMap.name);

         AbstractMap abMap = ourRightMap.GetComponentInChildren<AbstractMap>();
         if (abMap == null)
            Debug.LogError("can't find the abstract map!");
         
         abMap.UpdateMap(new Vector2d(placeInfo.lat, placeInfo.lon), 16.0f);        

         GetComponent<SchematicMapManager>().SelectMap(0); 
         ourLoader.TurnAllOff();
      }

      Touched();
      activePlaceMark = this.gameObject;
   }

   public static void UnSelect() //TODO, avoid static methods. move this kind of functionality up into a "Places Manager", probably retool LoadPlaceJSON
   {
      activePlaceMark = null;
      UnTouched();
      
   }

   public void SetupTexture()
   {
      GameObject icon = transform.Find("IconHolder").gameObject; //TODO: null checks!

      Texture2D texture = Resources.Load<Texture2D>(placeInfo.icon);
      icon.GetComponent<Renderer>().material.SetTexture("_MainTex", texture); //setup icon
      icon.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(1, -1)); //flip texture

      int w = texture.width; //TODO this may be inccorect (wrong aspect ratio?)
      int h = texture.height;

      Vector3 newScale = icon.transform.localScale;

      if (w > h)
      {
         newScale.z = newScale.x * (float)h / (float)w;
      }
      else
      {
         newScale.x = newScale.y * (float)w / (float)h;
      }
      icon.transform.localScale = newScale;

      GameObject text = transform.Find("Text").gameObject; //TODO: null checks!
      text.GetComponent<TMPro.TextMeshPro>().text = placeInfo.name;
   }

   public void ComputeMoreExactTargetPosition()
   {
      Ray r = new Ray(targetPosition + new Vector3(0f, 1f, 0f), Vector3.down);
      RaycastHit ourHit;
      bool hit = Physics.Raycast(r, out ourHit, Mathf.Infinity, 1 << 12); // we really only need to do this once. 
      if (hit)
      {
         targetPosition = ourHit.point; //get more accurate y intersection using raycast to collider.
      }
   }

   public void SetupPointer()
   {
      //print("setting up pointer!");      

      Vector3 pos2 = targetPosition;

      Vector3 pos = this.transform.position;

      //print("setting up point for: " + this.name + " pos: " + pos + " target: " + pos2);
      GameObject icon = transform.Find("IconHolder").gameObject; //TODO: null checks!
      pos.y -= icon.GetComponent<Renderer>().bounds.extents.y;


      Vector3 centerPoint = (pos + pos2) * 0.5f;

      ourPointer.transform.parent = null; //unparent to prevent scaling issues

      ourPointer.transform.position = centerPoint;

      float scaledLength = (pos2 - pos).magnitude;
      ourPointer.transform.localScale = new Vector3(0.01f, 0.01f, scaledLength);
      ourPointer.transform.localRotation = Quaternion.LookRotation((pos2 - pos).normalized);

      //icon.transform.Translate(0.0f, 0.008f, 0.0f); //push icons just infront of pointers

      ourPointer.transform.parent = transform; //reparent now that we are setup
   }

   public void LoadReadingListJSON()
   {
      if (placeInfo.menu == "")
         return;

      TextAsset txt = Resources.Load<TextAsset>(placeInfo.menu); //ommit .json extension
      if (txt == null)
      {
         Debug.LogError("couldn't load menu text file!");
         return;
      }
      string filecontent = txt.text;
      ourMenu = JsonUtility.FromJson<WholeMenu>(filecontent);

      GetComponent<SchematicMapManager>().Setup();
   }
}
