﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;

public class ExcavationController : MonoBehaviour {
   GameObject ourCube;
   GameObject ourText;
   bool isShown = false;
   public GameObject zoomMap;

	// Use this for initialization
	void Start () {
      ourCube = transform.Find("Cube").gameObject;
      ourText = transform.Find("Text").gameObject;

      if(ourCube==null)
      {
         Debug.LogError("Excavation Toggle: couldn't find our Cube. did the names change?");
      }
      if (ourText == null)
      {
         Debug.LogError("Excavation Toggle: couldn't find our Text. did the names change?");
      }

      Refresh();

   }
	
	// Update is called once per frame
	void Update () {
		
	}

   void Refresh()
   {
      if (isShown)
      {
         ourText.GetComponent<TMPro.TextMeshPro>().text = "Excavations: On";
         ourText.GetComponent<TMPro.TextMeshPro>().color = Color.red;
      }
      else
      {
         ourText.GetComponent<TMPro.TextMeshPro>().text = "Excavations: Off";
         ourText.GetComponent<TMPro.TextMeshPro>().color = Color.black;
      }

      IVectorDataLayer ivd=zoomMap.GetComponent<AbstractMap>().VectorData;      
      VectorSubLayerProperties vprop=ivd.GetFeatureSubLayerAtIndex(0);
      vprop.SetActive(isShown);

   }

   public void Clicked()
   {
      if (isShown) isShown = false;
      else         isShown = true;

      Refresh();
   }

   public void Touched(GameObject g)
   {
      ourCube.GetComponent<Renderer>().material.color = Color.magenta;
   }

   public void Untouched() 
   {
      ourCube.GetComponent<Renderer>().material.color = Color.white;
   }
}
