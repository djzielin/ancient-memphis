﻿using UnityEngine;
using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration.Data;
using Mapbox.Utils;
using Mapbox.Unity.Utilities;


using System; //to get double based math functions

public class GetMapBounds
{
    public GameObject ourMapGameObject;
    private AbstractMap _map;
    double[] y;
    double[] x;

    private Vector2d[] pos_as_latlong;

    bool arraysCreated = false;

    public GetMapBounds(GameObject goMap)
    {
        ourMapGameObject = goMap;
        _map = ourMapGameObject.GetComponent<AbstractMap>();

        if (arraysCreated == false)
        {
            y = new double[2];
            x = new double[2];

            pos_as_latlong = new Vector2d[4];
            arraysCreated = true;
        }
    }

    double tripleMax(double a, double b, double c)
    {
        return Math.Max(a, Math.Max(b, c));
    }

    double tripleMin(double a, double b, double c)
    {
        return Math.Min(a, Math.Min(b, c));
    }

    public Vector2d[] CalcBounds()
    {
        Debug.Log("looking at " + ourMapGameObject.transform.childCount + " children to calculate bounds");
        for (int i = 0; i < ourMapGameObject.transform.childCount; i++) //transform.childCount
        {
            GameObject g = ourMapGameObject.transform.GetChild(i).gameObject;

            UnityTile ut = g.GetComponent<UnityTile>();
            if (ut == null)
                continue;

            //print("ID: " + ut.CanonicalTileId);
            //print("Rect Max: " + ut.Rect.Max + " Min: " + ut.Rect.Min);
            //print("Center: " + Conversions.MetersToLatLon(ut.Rect.Center));
            //print("Rect Max: " + Conversions.MetersToLatLon(ut.Rect.Max) + " Min: " + Conversions.MetersToLatLon(ut.Rect.Min));

            if (i == 0)
            {
                y[0] = Math.Max(ut.Rect.Max.y, ut.Rect.Min.y);
                y[1] = Math.Min(ut.Rect.Max.y, ut.Rect.Min.y);
                x[0] = Math.Max(ut.Rect.Max.x, ut.Rect.Min.x);
                x[1] = Math.Min(ut.Rect.Max.x, ut.Rect.Min.x);
            }
            else
            {

                y[0] = tripleMax(y[0], ut.Rect.Max.y, ut.Rect.Min.y);
                y[1] = tripleMin(y[1], ut.Rect.Max.y, ut.Rect.Min.y);
                x[0] = tripleMax(x[0], ut.Rect.Max.x, ut.Rect.Min.x);
                x[1] = tripleMin(x[1], ut.Rect.Max.x, ut.Rect.Min.x);
            }
        }

        Vector2d[] pos_as_meters = new Vector2d[4];

        pos_as_meters[0] = new Vector2d(x[0], y[0]);
        pos_as_meters[1] = new Vector2d(x[0], y[1]);
        pos_as_meters[2] = new Vector2d(x[1], y[1]);
        pos_as_meters[3] = new Vector2d(x[1], y[0]);


        //print("x[0]: " + x[0]);
        //print("y[0]: " + y[0]);
        //print("x[1]: " + x[1]);
        //print("y[1]: " + y[1]);

        pos_as_latlong = new Vector2d[4];

        for (int i = 0; i < 4; i++)
        {
            pos_as_latlong[i] = Conversions.MetersToLatLon(pos_as_meters[i]);
        }

        return pos_as_latlong;
    }
}
