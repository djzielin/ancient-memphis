﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Mapbox;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;

public class PutOnGlobe : MonoBehaviour
{
    public float lat;
    public float lon;
    public AbstractMap _map;
    bool setup = false;
    Transform originalParent;

    public GameObject mainInfoSphere;
   

    void Start()
    {
        if (setup == false)
        {
            Vector2d latlong = new Vector2d(lat, lon);
            var earthRadius = ((IGlobeTerrainLayer)_map.Terrain).EarthRadius;
            Vector3 pos = Conversions.GeoToWorldGlobePosition(latlong, earthRadius * _map.gameObject.transform.localScale.x);
            this.transform.position = pos + _map.gameObject.transform.position;
            this.transform.RotateAround(_map.gameObject.transform.position, Vector3.up, _map.gameObject.transform.rotation.eulerAngles.y);
            this.transform.parent = _map.gameObject.transform;
            setup = true;
        }

    }
}

   
