﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine.UI;

public class SchematicMapManager : MonoBehaviour
{
   public GameObject ourWallMap;
   public GameObject ourSchematicMaps;
   public GameObject rightWallMap;
   public ItemSelection ourItemSelectionScript; //TODO: do this cleaner. we are trying to get at the zoom map at a round a bout way. 
   public Material selectedMaterial;
   public GameObject leftPanel;
   public GameObject boxOutline;
   public GameObject hoverText;

   PlaceMark ourPlaceMark;

   //GameObject ourSchematicMapRoot;
   public GameObject[] physicalMaps;
   //GameObject[] physicalMenuItems;
   //GameObject[] physicalIcons;
   //GameObject[] physicalText;
   GameObject[] hotspots;

   int numberOfMaps;
   int currentlySelectedMap;
   GameObject hotspotTouched;
   GameObject ourMenuText;
   GameObject ourImage;

   public void SelectMap(int which)
   {
      ourSchematicMaps.transform.rotation = new Quaternion(); //reset rotation when coming back for a look

      for (int i = 0; i < numberOfMaps; i++)
      {
         if (i == which)
         {
            print("turning on map: " + i + " name: " + physicalMaps[i].name);
            physicalMaps[i].SetActive(true);
            //physicalIcons[i].GetComponent<Renderer>().material.color = Color.magenta;
         }
         else
         {
            physicalMaps[i].SetActive(false);
            //physicalIcons[i].GetComponent<Renderer>().material.color = Color.white;
         }
      }

      ourMenuText.GetComponentInChildren<TMPro.TextMeshPro>().text = ourPlaceMark.ourMenu.schematic_maps[which].name;

      leftPanel.SetActive(true);
      leftPanel.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = ourPlaceMark.ourMenu.schematic_maps[which].wall_text;
      ourImage.SetActive(false);

      ourItemSelectionScript.zoomMap.GetComponentInChildren<AbstractMap>().UpdateMap(new Vector2d(ourPlaceMark.ourMenu.schematic_maps[which].lat, ourPlaceMark.ourMenu.schematic_maps[which].lon), 18.0f);
      ourItemSelectionScript.rightwallMapBoundsUpdater.ForceRefresh();

      currentlySelectedMap = which;

      IVectorDataLayer ivd = rightWallMap.GetComponent<AbstractMap>().VectorData;

      print("setting up the shapefile visibility now");


      IEnumerable<VectorSubLayerProperties> allSublayers = ivd.GetAllFeatureSubLayers();
      ICollection<VectorSubLayerProperties> c = allSublayers as ICollection<VectorSubLayerProperties>;
      print("computed we have " + c.Count + " shapefiles");

      for (int i = 0; i < c.Count; i++)
      {
         VectorSubLayerProperties vprop = ivd.GetFeatureSubLayerAtIndex(i);
        
         if (i == ourPlaceMark.ourMenu.schematic_maps[0].vector_layer) //turn on the requested shapefile layer
         {
            print("  setting layer: " + i + " to: true");
            vprop.SetActive(true);
         }
         else //all other layers off
         {
            vprop.SetActive(false);
            print("  setting layer: " + i + " to: false");
         }

         HotspotUnTouched();
      }
   }

   public void UnSelect()
   {
      for (int i = 0; i < numberOfMaps; i++)
      {
         physicalMaps[i].SetActive(false);
         //physicalIcons[i].GetComponent<Renderer>().material.color = Color.white;
      }

      currentlySelectedMap = -1;
   }

   // Use this for initialization
   void Start()
   {
      ourMenuText = GameObject.Find("Menu Text");
      ourImage = leftPanel.transform.Find("Image").gameObject;
   }

   public void TurnOff()
   {
      UnSelect();
      HotspotUnTouched();
      boxOutline.SetActive(false);

      //ourSchematicMapRoot.SetActive(false);
   }

   int LocateHotSpot(GameObject g)
   {
      int numSpots = ourPlaceMark.ourMenu.schematic_maps[currentlySelectedMap].hot_spots.Count;
      int spotNum = -1;
      for (int e = 0; e < numSpots; e++)
      {
         string n = ourPlaceMark.ourMenu.schematic_maps[currentlySelectedMap].hot_spots[e].hover_text;

         if (n == g.name)
         {
            //print("we are dealing with hotspot: " + e);
            spotNum = e;
            break;
         }
      }

      return spotNum;
   }

   static Vector2 ComputeImageSize(float aspectRatio, Vector2 targetSize)
   {
      float computedH;
      float computedW;
      
      float inv_aspectRatio = 1 / aspectRatio;

      computedW = targetSize.x;
      computedH = computedW * inv_aspectRatio;
      
      if (computedH > targetSize.y)
      {
         print("still too big!");
         float scale = targetSize.y / computedH;

         computedW = computedW * scale;
         computedH = computedH * scale;
      }
      //print("computed size. w: " + computedW + " h: " + computedH);

      return new Vector2(computedW, computedH);
   }

   static public void SetupImage(string imageFileName, float aspectRatio, GameObject physicalImage)
   {
      if (imageFileName != "")
      {
         Texture2D texture = Resources.Load<Texture2D>(imageFileName);
         if (texture == null)
         {
            Debug.LogError("something went wrong loading the panel image texture. is the filename and path correct?");
            return;
         }

         physicalImage.GetComponent<RawImage>().texture = texture; //setup image
         print("setting up: " + imageFileName);

         Vector2 computedSize = ComputeImageSize(aspectRatio, new Vector2(1.2f, 0.8f));

         physicalImage.GetComponent<RawImage>().color = Color.white;
         physicalImage.GetComponent<RectTransform>().sizeDelta = computedSize;
         physicalImage.SetActive(true);
      }
      else
      {
         physicalImage.GetComponent<RawImage>().texture = null;
         physicalImage.GetComponent<RawImage>().color = Color.black;
         physicalImage.SetActive(false);
      }
   }


   public void HotspotClicked(GameObject g)
   {
      leftPanel.SetActive(true);

      int spotNum = LocateHotSpot(g);

      if (spotNum == -1)
      {
         Debug.LogError("Can't find the hotspot info for: " + g.name);
         return;
      }
      SingleHotSpot shs = ourPlaceMark.ourMenu.schematic_maps[currentlySelectedMap].hot_spots[spotNum];

      leftPanel.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = shs.wall_text;
      
      string imageFileName = ourPlaceMark.ourMenu.schematic_maps[currentlySelectedMap].hot_spots[spotNum].image;
      float aspectRatio = ourPlaceMark.ourMenu.schematic_maps[currentlySelectedMap].hot_spots[spotNum].aspect_ratio;

      SetupImage(imageFileName, aspectRatio, ourImage);

      ourItemSelectionScript.zoomMap.GetComponentInChildren<AbstractMap>().UpdateMap(new Vector2d(shs.lat, shs.lon), 18.0f);
      ourItemSelectionScript.rightwallMapBoundsUpdater.ForceRefresh();

      SetupBox(g);
   }

   void SetupBox(GameObject g)
   {
      boxOutline.SetActive(true);
      boxOutline.transform.parent = g.transform;
      boxOutline.transform.localScale = g.GetComponent<BoxCollider>().size;
      boxOutline.transform.localRotation = new Quaternion();
      boxOutline.transform.localPosition = g.GetComponent<BoxCollider>().center;

      Vector3[] allPositions = new Vector3[boxOutline.transform.childCount];
      GameObject[] allChildren = new GameObject[boxOutline.transform.childCount];

      for (int i = 0; i < boxOutline.transform.childCount; i++)
      {
         GameObject bc = boxOutline.transform.GetChild(i).gameObject;
         allPositions[i] = bc.transform.position;
         allChildren[i] = bc;
      }

      for(int i=0;i<allChildren.Length;i++)
      {
         GameObject bc = allChildren[i];
         bc.transform.parent = null;
         bc.transform.localScale = new Vector3(0.005f,1.00f, 0.005f); //put back to 0.5 (instead of 1.0) if return to capsule style
         bc.transform.parent = boxOutline.transform;

         Vector3 newScale = bc.transform.localScale;
         newScale.y = 1.00f; //always have 1.0 length //put back to 0.5 (instead of 1.0) if return to capsule style
         bc.transform.localScale = newScale;
         bc.transform.position = allPositions[i];
      }
   }

   public void HotspotTouched(GameObject g)
   {
      if (hotspotTouched == g)
         return;

      HotspotUnTouched();

      g.GetComponent<Renderer>().enabled = true;
      hotspotTouched = g;
      hoverText.SetActive(true);

      int spotNum = LocateHotSpot(g);
      if (spotNum != -1)
      {
         hoverText.transform.parent = g.transform;
         hoverText.transform.localPosition = g.GetComponent<BoxCollider>().center + new Vector3(0f, 0f, -5f);
         hoverText.GetComponent<TMPro.TextMeshPro>().text = ourPlaceMark.ourMenu.schematic_maps[currentlySelectedMap].hot_spots[spotNum].hover_text;
         hoverText.GetComponent<TMPro.TextMeshPro>().color = Color.magenta;
      }
   }

   public void HotspotUnTouched() //this name is bad
   {
      if (hotspotTouched != null)
      {
         hotspotTouched.GetComponent<Renderer>().enabled = false;
         hotspotTouched = null;
         hoverText.SetActive(false);
      }
   }   

   public void Setup()
   {
      ourPlaceMark = GetComponent<PlaceMark>();
      print("trying to setup schematic map for: " + ourPlaceMark.name);

      //GameObject prototype = ourWallMap.transform.Find("prototype schematic map root").gameObject;

      /*if (prototype == null)
      {
         Debug.LogError("no prototype schematic map root found! Did you change the name?");
         return;
      }*/

      //ourSchematicMapRoot = Instantiate(prototype, prototype.transform.parent);
      //ourSchematicMapRoot.name = ourPlaceMark.ourMenu.name;

      numberOfMaps = ourPlaceMark.ourMenu.schematic_maps.Count;

      physicalMaps = new GameObject[numberOfMaps];
      //physicalMenuItems = new GameObject[numberOfMaps];
      //physicalText = new GameObject[numberOfMaps];
      //physicalIcons = new GameObject[numberOfMaps];

      /*GameObject physicalMapPrototype = ourSchematicMapRoot.transform.Find("prototype actual map").gameObject;
      if (physicalMapPrototype == null)
      {
         Debug.LogError("can't find the actual map prototype in the hierarchy, did you change the name?");
         return;
      }

      GameObject physicalMenuItemPrototype = ourSchematicMapRoot.transform.Find("prototype menu item").gameObject;
      if (physicalMenuItemPrototype == null)
      {
         Debug.LogError("can't find the menu item prototype  in the hierarchy, did you change the name?");
         return;
      }*/

      for (int i = 0; i < ourPlaceMark.ourMenu.schematic_maps.Count; i++)
      {
         //////////////////////////////
         // Setup 3D Schematic Map
         //////////////////////////////

         GameObject map = ourSchematicMaps.transform.Find(ourPlaceMark.ourMenu.schematic_maps[i].model).gameObject;
         if (map == null)
         {
            Debug.LogError("can't find the schematic map");
            return;
         }
         physicalMaps[i] = map;

         ///////////////////////
         // Setup Menu Item
         ///////////////////////
         /*
         GameObject ourPhysicalMenuItem = Instantiate(physicalMenuItemPrototype, physicalMenuItemPrototype.transform.parent);
         ourPhysicalMenuItem.name = "Menu " + i.ToString();
         physicalMenuItems[i] = ourPhysicalMenuItem;
         ourPhysicalMenuItem.SetActive(true);

         GameObject ourText= ourPhysicalMenuItem.transform.Find("text").gameObject;
         if(ourText==null)
         {
            Debug.LogError("can't find text object in menu item. did the name change?");
            return;
         }
         ourText.GetComponent<TextMesh>().text = ourPlaceMark.ourMenu.schematic_maps[i].name;
         ourPhysicalMenuItem.transform.Translate(0, -0.25f * (float)i, 0f);
         physicalText[i] = ourText;

         Texture2D iconTexture = Resources.Load<Texture2D>(ourPlaceMark.ourMenu.schematic_maps[i].icon);

         if (iconTexture == null)
         {
            Debug.LogError("something went wrong loading the icon map texture. is the filename and path correct?");
            return;
         }

         GameObject ourIcon = ourPhysicalMenuItem.transform.Find("icon").gameObject;
         ourIcon.GetComponent<Renderer>().material.SetTexture("_MainTex", iconTexture); //setup icon

         int w = iconTexture.width;
         int h = iconTexture.height;

         Vector3 newScale = ourIcon.transform.localScale;

         if (w > h)
         {
            newScale.z = newScale.x * (float)h / (float)w;
         }
         else
         {
            newScale.x = newScale.y * (float)w / (float)h;
         }
         ourIcon.transform.localScale = newScale;
         physicalIcons[i] = ourIcon;
         */
         ///////////////////////
         // HOTSPOT SETUP
         //////////////////////

         int numSpots = ourPlaceMark.ourMenu.schematic_maps[i].hot_spots.Count;
         hotspots = new GameObject[numSpots];
         for (int e = 0; e < numSpots; e++)
         {
            string colliderName = ourPlaceMark.ourMenu.schematic_maps[i].hot_spots[e].collider;
            if (colliderName == "")
               continue;

            Transform t = map.transform.Find(colliderName);

            if (t == null)
            {
               Debug.LogError("Can't find collider named: " + colliderName);
               continue;
            }
            GameObject ourCollider = t.gameObject;

            ourCollider.name = ourPlaceMark.ourMenu.schematic_maps[i].hot_spots[e].hover_text;
            ourCollider.AddComponent<BoxCollider>();
            ourCollider.GetComponent<Renderer>().enabled = false;
            ourCollider.layer = LayerMask.NameToLayer("HotSpot");

            Color fixedColor = Color.magenta;
            fixedColor.a = 0.5f;

            selectedMaterial.color = fixedColor;


            ourCollider.GetComponent<Renderer>().material = selectedMaterial;

            hotspots[e] = ourCollider; //store for later
         }
         map.SetActive(false);
      }

      currentlySelectedMap = 0;
   }
}
