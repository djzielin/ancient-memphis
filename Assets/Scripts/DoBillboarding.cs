﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoBillboarding : MonoBehaviour {

	// Use this for initialization
	void Start () {

   }

   private void Update()
   {
      if (Camera.main == null) //cameras not setup yet
         return;

      Vector3 camPos = Camera.main.transform.position;

      Vector3 ourPos = transform.position;
      if (GetComponent<BoxCollider>() !=null)
            ourPos = transform.TransformPoint(GetComponent<BoxCollider>().center); //get this as world space

      camPos.y = 0; //don't want to pitch text
      ourPos.y = 0;

      Vector3 direction = (ourPos - camPos).normalized;

      transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
   }

}
