﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.MeshGeneration.Factories.TerrainStrategies;
using Mapbox.Unity.Map;
using Mapbox.Unity.MeshGeneration.Factories;

public enum BuildType { OculusGo, Vive, WebGL };

//TODO figure out different way to do this
public class WaitForMapsToBeReady : MonoBehaviour
{
   public bool isReady = false;

   public GameObject ourCanvas;
   public GameObject ourOverlay;
   public GameObject worldHidingSphere;

   public GameObject rightViewportCamera;
   public GameObject leftViewportCamera;

   private GameObject TableMapUnscaled; //get the refs to these using GameObject.Find
   private GameObject WallMapRightUnscaled;
   private GameObject GlobeMapUnscaled;
   private GameObject WallMapFrontUnscaled;
   private LoadPlacesJSON ourPlacesLoader;
     
   private bool doCanvas;

   public BuildType ourBuildType;

   public string GetBuildType()
   {
      if (ourBuildType == BuildType.OculusGo)
         return "OculusGo";
      if (ourBuildType == BuildType.Vive)
         return "Vive";
      if (ourBuildType == BuildType.WebGL)
         return "WebGL";

      return "NONE";
   }

   private void Awake()
   {
      ourPlacesLoader = GetComponent<LoadPlacesJSON>();
      TableMapUnscaled = GameObject.Find("TableMap Unscaled");
      WallMapRightUnscaled = GameObject.Find("WallMapRight Unscaled");
      GlobeMapUnscaled = GameObject.Find("GlobeMap Unscaled");
      WallMapFrontUnscaled = GameObject.Find("WallMapFront Unscaled");

      if (ourBuildType == BuildType.WebGL)
         doCanvas = true;
      else
         doCanvas = false;

      if (doCanvas)
      {
         ourCanvas.SetActive(true);
         ourOverlay.SetActive(false);
         GameObject.Find("LeftHandAnchor").SetActive(false);
		 GameObject.Find("RightHandAnchor").SetActive(false);
      }
      else
      {
         ourCanvas.SetActive(false);
         ourOverlay.SetActive(false); //oculus overlay takes 1 frame till active, kind of ruins the point of using as startup splash. perhaps a bug that will be fixed in next version?

         GameObject ovr = GameObject.Find("OVRCameraRig");
         worldHidingSphere.transform.parent = ovr.transform;
         worldHidingSphere.transform.localPosition = new Vector3(0f, 0f, 0f);
         worldHidingSphere.SetActive(true);

         if (ourBuildType == BuildType.Vive)
         {
            ovr.transform.position = new Vector3(0f, 0f, 0.2f);
         }
      }
   }

   void Start ()
   { 
      StartCoroutine(WaitSomeFrames());
   }   

   void SetLayerID(GameObject g, int layer)
   {
      for (int i = 0; i < g.transform.childCount; i++) //get layers setup properly for raycast onto table map
      {
         GameObject child = g.transform.GetChild(i).gameObject;
         if (child.name != "SelectionSphere")
            child.layer = layer;
      }
   }

   IEnumerator WaitForTilesLoaded(GameObject g)
   {
      for (int i = 0; i < TableMapUnscaled.transform.childCount; i++)
      {
         GameObject child = TableMapUnscaled.transform.GetChild(i).gameObject;

         if (child.activeInHierarchy == false)
            continue;

         Mapbox.Unity.MeshGeneration.Data.UnityTile ut = child.GetComponent<Mapbox.Unity.MeshGeneration.Data.UnityTile>();
         if (ut == null)
            continue; //object doesn't have a tile with it, lets skip

         while (ut.RasterDataState != Mapbox.Unity.MeshGeneration.Enums.TilePropertyState.Loaded)
            yield return null; //wait for load
      }
   }

    IEnumerator WaitSomeFrames()
    {
      print("Load Started at Time: " + Time.time);

      for (int i=0;i<5;i++) //wait 5 frames?
        {
            yield return null;
        }

      SetLayerID(TableMapUnscaled, 12);
      SetLayerID(WallMapRightUnscaled, 12);


      yield return StartCoroutine(WaitForTilesLoaded(TableMapUnscaled));
      yield return StartCoroutine(WaitForTilesLoaded(WallMapRightUnscaled));
      yield return StartCoroutine(WaitForTilesLoaded(GlobeMapUnscaled));
      yield return StartCoroutine(WaitForTilesLoaded(WallMapFrontUnscaled));

      print("Map Load Finished at Time: " + Time.time);

      ourPlacesLoader.enabled = true;

      while(ourPlacesLoader.setup==false)
         yield return null; //wait for load

      print("Place JSON Load Finished at Time: " + Time.time);
      isReady = true;

      WallMapRightUnscaled.transform.parent.gameObject.SetActive(false);

      if (doCanvas)
      {
         print("Setting up 2D mode");
         Camera.main.rect = new Rect(0.3f, 0f, 0.4f, 1.0f);
         Camera.main.transform.parent.parent.Translate(0f, 0f, -0.5f);

         leftViewportCamera.SetActive(true);
         rightViewportCamera.SetActive(true);

         GameObject leftPanel = GameObject.Find("LeftPaneText");
         GameObject rightPanel = GameObject.Find("RightPaneText");
         GameObject wallMapRight = WallMapRightUnscaled.transform.parent.gameObject;

         leftPanel.transform.position = new Vector3(-2f, 1.3f, -0.5f);
         rightPanel.transform.position = new Vector3(2f, 1.3f, -0.5f);
         wallMapRight.transform.position = new Vector3(2f, 1.5f, -0.66f);

         leftPanel.transform.rotation = Quaternion.Euler(0f, -90f, 0f);
         rightPanel.transform.rotation = Quaternion.Euler(0f, 90f, 0f);
         wallMapRight.transform.rotation = Quaternion.Euler(-90f, 90f, 0f);

         GameObject.Find("OVRCameraRig").transform.position = new Vector3(0, 1.5f, 0.2f);
      }

      ourCanvas.SetActive(false); //hide anything hiding the scene
      ourOverlay.SetActive(false);
      worldHidingSphere.SetActive(false);
   }


    // Update is called once per frame
    void Update()
    {
       
    }
}
